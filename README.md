# Alumni Network Backend

##### Alumni Network - Live application : [![Live Application](https://img.shields.io/badge/-CLICK_HERE-1f425f.svg)](https://lucaspersson.gitlab.io/alumni-network)

A Hibernate and Spring Boot application in Java. Connected with Alumni Network Frontend : [![FRONTENDAPP](https://img.shields.io/badge/-CLICK_HERE-1f425f.svg)](https://gitlab.com/LucasPersson/alumni-network)

![build](https://img.shields.io/badge/build-passing-green)

![keycloak](https://img.shields.io/badge/keycloak_version-^17.0.0-blue)
![spring](https://img.shields.io/badge/spring_boot_version-2.6.4-blue)
![jdk](https://img.shields.io/badge/jdk_version-17-blue)
![gradle](https://img.shields.io/badge/openapi_version-1.3.3-blue)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Testing](#testing)
- [Assumptions](#assumptions)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

This project is created for the purpose of creating a backend API for our case Alumni Network.

## Install

```
git clone https://gitlab.com/LucasPersson/alumni-network-backend
cd alumni-network-backend
```

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Usage

[![windows](https://img.shields.io/badge/--6fa8dc?logo=windows&logoColor=000)](https://www.microsoft.com/sv-se/windows)
[![jira](https://img.shields.io/badge/--005bf7?logo=jira&logoColor=000)](https://www.atlassian.com/software/jira)
[![spring](https://img.shields.io/badge/--8fce00?logo=spring&logoColor=000)](https://spring.io/)
[![Java](https://img.shields.io/badge/--f44336?logo=java&logoColor=000)](https://www.oracle.com/se/java/)
[![intellij](https://img.shields.io/badge/--e857ac?logo=intellijidea&logoColor=000)](https://www.jetbrains.com/idea/)
[![swagger](https://img.shields.io/badge/--44d84a?logo=swagger&logoColor=000)](https://alumni-be.herokuapp.com/swagger-ui/index.html#/)
[![hibernate](https://img.shields.io/badge/--e3db54?logo=hibernate&logoColor=000)](https://hibernate.org/)
[![gradle](https://img.shields.io/badge/--31cfff?logo=gradle&logoColor=000)](https://gradle.org/)
[![docker](https://img.shields.io/badge/--3186ff?logo=docker&logoColor=000)](https://www.javascript.com/)
[![heroku](https://img.shields.io/badge/--9d1ef2?logo=heroku&logoColor=000)](https://alumni-be.herokuapp.com/)
[![postgresql](https://img.shields.io/badge/--6fa8dc?logo=postgresql&logoColor=000)](https://www.postgresql.org/)
[![keycloak](https://img.shields.io/badge/-Keycloak-white.svg)](https://www.keycloak.org/)


For linux/mac users, open a terminal and run:

```sh
./gradlew bootRun
```

For Windows users, use `gradlew.bat` instead of `gradlew` in PowerShell.

```
Open IntelliJ IDEA.
heroku open
```


## Swagger

[![swagger](https://img.shields.io/badge/Open%20Swagger-HERE-green.svg)](https://alumni-be.herokuapp.com/swagger-ui/index.html#/)
```
-> Authorize

Username: swagger
Password: swagger

-> Access data

```

## Testing

For testing the endpoint of the application you can use the Swagger documentation UI [![swagger](https://img.shields.io/badge/-HERE-green.svg)](https://alumni-be.herokuapp.com/swagger-ui/index.html#/)

## API Documentation

[![application](https://img.shields.io/badge/Link_to_API_Documentation-HERE-orange.svg)](https://gitlab.com/LucasPersson/alumni-network-backend/-/wikis/uploads/fcc63fbfcd30eba7e5bb7da164628d09/API_DOCUMENTATION.pdf)

## ER - Diagram

[![ERD](https://img.shields.io/badge/Link_to_ERD-HERE-orange.svg)](https://gitlab.com/LucasPersson/alumni-network-backend/-/wikis/uploads/7f1685b43d0a1104507d50fec56e4ed7/ERD_-_Alumni_Network.drawio.png)


## Assumptions

In the assignment we have made the following assumptions.

## Maintainers

[![GitLab](https://badgen.net/badge/icon/Lucas%20Persson?icon=gitlab&label)](https://gitlab.com/LucasPersson)
[![GitLab](https://badgen.net/badge/icon/Christian%20Neij?icon=gitlab&label)](https://gitlab.com/ChristianNeij)
[![GitLab](https://badgen.net/badge/icon/Zahra%20Ghadban?icon=gitlab&label)](https://gitlab.com/zizighadban)
[![GitLab](https://badgen.net/badge/icon/Anna%20Hallberg?icon=gitlab&label)](https://gitlab.com/haruberi)

## Mentor

[![GitLab](https://badgen.net/badge/icon/Nicholas%20Lennox?icon=gitlab&label)](https://gitlab.com/NicholasLennox)

## Contributing

PRs accepted.

* [![standard-readme compliant](https://img.shields.io/badge/standard_readme-HERE-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

* [![standard-readme compliant](https://img.shields.io/badge/readme_badges-HERE-green.svg?style=flat-square)](https://github.com/Naereen/badges/blob/master/README.md)

## License

MIT License © 2022 Lucas Persson, Christian Neij, Zahra Ghadban, Anna Hallberg
