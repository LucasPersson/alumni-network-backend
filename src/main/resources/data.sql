-- AlumniUser
INSERT INTO alumni_user(username, name, bio, fun_fact, key_token_id, status)
VALUES('amanda', 'Amanda Larsson', 'Happy girl living in Italy', 'It takes almost three years for a pineapple to grow.', '6c54ac49-3672-425a-bb5b-b50402a0b786', 'teaching');

INSERT INTO alumni_user(username, bio, fun_fact, key_token_id, status)
VALUES('caleb', 'Knicks rule', 'King Tut was buried with a dagger made of iron forged from a meteor.', '8wa94aaw66', 'looking at birds');

INSERT INTO alumni_user(username, name, bio, fun_fact, key_token_id, status)
VALUES('lily', 'Lillian Flowerpod', 'Currently studying to become a botanist', 'There are red bananas that taste sweet, creamy and a little bit like raspberries.', '21d84dddw0', 'Currently trying to win a badger');

INSERT INTO alumni_user(username, name, bio, fun_fact, key_token_id, status)
VALUES('monica', 'Monica Plow', 'I am all about animals, yoga and nature', 'When a bald eagle loses a feather on one wing, it will lose a feather on the other wing just to keep its balance', '401dc4c8-0502-4db6-8360-c2f5f62ae83d', 'writing exams');

INSERT INTO alumni_user(username, bio, fun_fact, key_token_id, status)
VALUES('kevin', 'Fancy seeing you here stalker', 'There is a phobia that somewhere, somehow a duck is watching you. It is called anatidaephobia', '4da48dd37', 'The fear that we are all going to die!');

--AlumniGroup
INSERT INTO alumni_group ( description, is_private, name) VALUES ('Students who studied Java together.',false,'Java');
INSERT INTO alumni_group ( description, is_private, name) VALUES ('Students who studied .NET together.', true, '.NET');
INSERT INTO alumni_group ( description, is_private, name) VALUES ('Students who studied C++ together.', true, 'C++');
INSERT INTO alumni_group ( description, is_private, name) VALUES ('Frontend discussions for Java/C# courses', false, 'Frontend');
INSERT INTO alumni_group ( description, is_private, name) VALUES ('For people who love pineapple on pizza.', false, 'Pineapple on pizza!!');
INSERT INTO alumni_group ( description, is_private, name) VALUES ('A group for members to learn more about the worlds exotic fruits.', false, 'Exotic fruits');
INSERT INTO alumni_group ( description, is_private, name) VALUES ('We are a group that chat and meet up at Sabaton concerts.', false, 'Sabaton fanclub');


--Topic
INSERT INTO topic (description, name) VALUES ('I have baked a cake.', 'Cake');
INSERT INTO topic (description, name) VALUES ('The new Spider-man movie is out!!! Should we go see it?','Movie');
INSERT INTO topic (description, name) VALUES ('What is your favorite food?','Food');
INSERT INTO topic (description, name) VALUES ('Beverages of all kinds.','Drinks');
INSERT INTO topic (description, name) VALUES ('Join and vote for Cola or Pepsi','Cola or Pepsi?');
INSERT INTO topic (description, name) VALUES ('We love plants.','Plants');
INSERT INTO topic (description, name) VALUES ('Food-lovers','Cooking');
INSERT INTO topic (description, name) VALUES ('Post anything here.','General');

--TopicMember
INSERT INTO topic_member (user_id, topic_id) VALUES (1,1);
INSERT INTO topic_member (user_id, topic_id) VALUES (2,2);
INSERT INTO topic_member (user_id, topic_id) VALUES (3,6);
INSERT INTO topic_member (user_id, topic_id) VALUES (1,6);

--GroupMember
INSERT INTO alumni_group_member (user_id, group_id) VALUES (1,1);
INSERT INTO alumni_group_member (user_id, group_id) VALUES (2,2);
INSERT INTO alumni_group_member (user_id, group_id) VALUES (3,4);

-- Post
INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id) VALUES('Is spring boot data jpa something we could use for the assignment? Or do we have to do it through jdbc?', 'JPA or JDBC?', '2022-03-12 14:52:46', 1, 1);

INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id, reply_parent_id) VALUES('JPA comes up next week, for this assignment it is not intended.', 'RE: JPA or JDBC?', '2022-03-13 14:52:46', 1, 3, 1);

INSERT INTO post(content, title, timestamp, alumniuser_id, target_user) VALUES('Are we allowed to work with the same people again now in backend if we worked with them once in frontend?', 'this is title', '2022-03-11 10:08:31', 2, 3);

INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id) VALUES('This post should not be visible to Amanda', 'Amanda should not see this', '2022-02-12 14:52:46', 2, 2);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('The cake is, in fact, not a lie ;)', 'My cake', '2022-03-12 20:21:46', 1, 1);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('Willem Dafoe was great in it!', 'New Spiderman film [SPOILER WARNING]', '2022-03-11 22:32:16', 2, 2);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id, reply_parent_id) VALUES('What an awesome cake Amanda!', 'RE: My cake', '2022-03-17 10:12:26', 1, 3, 5);

INSERT INTO post(content, title, timestamp, alumniuser_id, target_user) VALUES('Yor cake is ugly and so are you.', 'Nasty cake', '2022-03-15 21:58:54', 4, 1);

INSERT INTO post(content, title, timestamp, alumniuser_id, reply_parent_id, topic_id) VALUES('I just want everyone in this topic to know, Monica is a hater go plow yourself!.', 'RE: My cake', '2022-03-15 21:58:54', 1, 5, 1);

INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id, reply_parent_id) VALUES('Okay, good to know, thanks!', 'RE: JPA or JDBC?', '2022-03-18 14:52:46', 1, 1, 1);

INSERT INTO post(content, title, timestamp, alumniuser_id, reply_parent_id, target_user) VALUES('Okay, good to know, thanks!', 'RE: Nasty cake', '2022-03-18 14:52:46', 1, 8, 4);

INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id) VALUES('Hey, I am having problems understanding store.js in vue 3. I do not understand when I am supposed to use state, when to use getters, when to use mutations and when to use actions.', 'Store.js in Vue 3 problems.', '2022-03-12 14:52:46', 4, 3);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('Dandelion is not a plant it is a beautiful flower.', 'Dandelion', '2022-03-11 22:32:16', 6, 3);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id, reply_parent_id) VALUES('Yes i think so to!', 'RE: Dandelion', '2022-03-11 22:32:16', 6, 3, 13);

INSERT INTO post(content, title, timestamp, alumniuser_id, target_user) VALUES('Hi Amanda i would like to give you a dandelion.', 'Flower', '2022-03-15 21:58:54', 3, 1);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('Dandelion is not a plant it is a beautiful flower.', 'Poppy', '2022-03-11 22:32:16', 6, 3);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('Hi everyone i would love to get som inspiration for my garden my favorite colours are orange and yellow.', 'Gardening', '2022-03-11 22:32:16', 6, 5);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('Hey, I am looking for a new book to read any suggestions.', 'Looking for a new book to read.', '2022-03-12 14:52:46', 8, 2);

INSERT INTO post(content, title, timestamp, topic_id, alumniuser_id) VALUES('Hey, I am trying to learn Python but i dont understand why i get compile errors everytime i try to close my line of code; HELP !!!!.', 'Trying to learn Python!', '2022-03-12 14:52:46', 8, 1);

INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id) VALUES('Hey, I am new to Java where do i start?.', 'Java newbie!', '2022-03-12 14:52:46', 1, 5);

INSERT INTO post(content, title, timestamp, alumnigroup_id, alumniuser_id) VALUES('Jvm is amazing.', 'JVM!', '2022-03-12 14:52:46', 1, 2);
