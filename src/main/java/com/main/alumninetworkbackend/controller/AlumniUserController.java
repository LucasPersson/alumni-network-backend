package com.main.alumninetworkbackend.controller;

import com.main.alumninetworkbackend.model.CommonResponse;
import com.main.alumninetworkbackend.model.dbo.AlumniUser;
import com.main.alumninetworkbackend.repository.AlumniUserRepository;
import com.main.alumninetworkbackend.util.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@Tag(name = "AlumniUser")
@RequestMapping("api/v1/")
@SecurityRequirement(name = "keycloak_implicit")
public class AlumniUserController {

    private final AlumniUserRepository alumniUserRepository;

    public AlumniUserController(AlumniUserRepository alumniUserRepository) {
        this.alumniUserRepository = alumniUserRepository;
    }


    public Map<String, String> getUserInfo(@AuthenticationPrincipal Jwt principal) {
        Map<String, String> map = new Hashtable<String, String>();
        map.put("user_name", principal.getClaimAsString("preferred_username"));
        map.put("userSubToken", principal.getClaimAsString("sub"));

        return Collections.unmodifiableMap(map);
    }

    @GetMapping("/alumniUser")
    public ResponseEntity<CommonResponse> getAlumniUser(HttpServletRequest request, @AuthenticationPrincipal Jwt principal){

        Command cmd = new Command(request);
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        Map<String, String> currentUserMap = getUserInfo(principal);
        String currentUserToken = currentUserMap.get("userSubToken");
        String currentUserUsername = currentUserMap.get("user_name");

        if(alumniUserRepository.existsAlumniUserByKeyTokenIdAndUsername(currentUserToken, currentUserUsername)) {
            commonResponse.data = alumniUserRepository.findAlumniUserByKeyTokenIdAndUsername(currentUserToken, currentUserUsername).get();
            commonResponse.message = "Alumni user";
            response = HttpStatus.OK;
        } else {

            AlumniUser alumniUser = new AlumniUser();

            alumniUser.keyTokenId = currentUserToken;
            alumniUser.username = currentUserUsername;
            alumniUserRepository.save(alumniUser);
            commonResponse.data = alumniUser;
            commonResponse.message = "Created an alumni user";
            response = HttpStatus.CREATED;

        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @GetMapping("/alumniUser/{id}")
    public ResponseEntity<CommonResponse> getAlumniUserById(HttpServletRequest request, @PathVariable int id){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(alumniUserRepository.existsById(id)) {
            commonResponse.data = alumniUserRepository.findById(id);
            commonResponse.message = "Alumni user ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Alumni user does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @PatchMapping("/alumniUser/{id}")
    public ResponseEntity<CommonResponse> updateAlumniUser(HttpServletRequest request, @RequestBody AlumniUser updateAlumniUser, @PathVariable int id, @AuthenticationPrincipal Jwt principal) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        Map<String, String> currentUserMap = getUserInfo(principal);
        String currentUserToken = currentUserMap.get("userSubToken");
        String currentUserUsername = currentUserMap.get("user_name");

        if(!alumniUserRepository.existsById(id)) {
            commonResponse.data = null;
            commonResponse.message = "User does not exist!";
            response = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(commonResponse, response);
        }

        Optional<AlumniUser> alumniUserRepositoryById = alumniUserRepository.findById(id);
        AlumniUser alumniUser = alumniUserRepositoryById.get();

        if(alumniUserRepository.existsAlumniUserByKeyTokenIdAndUsername(currentUserToken, currentUserUsername) && Objects.equals(alumniUser.username, currentUserUsername) && Objects.equals(alumniUser.keyTokenId, currentUserToken)) {

            if (updateAlumniUser.name != null) {
                alumniUser.name = updateAlumniUser.name;
            }
            if (updateAlumniUser.status != null) {
                alumniUser.status = updateAlumniUser.status;
            }
            if (updateAlumniUser.bio != null) {
                alumniUser.bio = updateAlumniUser.bio;
            }
            if (updateAlumniUser.funFact != null) {
                alumniUser.funFact = updateAlumniUser.funFact;
            }

            alumniUserRepository.save(alumniUser);

            commonResponse.data = alumniUser;
            commonResponse.message = "Updated alumni user with ID: " + alumniUser.id;
            response = HttpStatus.OK;

        } else {

            commonResponse.message = "You may not modify this user!";
            response = HttpStatus.FORBIDDEN;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }
}
