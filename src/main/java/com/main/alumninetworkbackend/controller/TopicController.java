package com.main.alumninetworkbackend.controller;

import com.main.alumninetworkbackend.model.CommonResponse;
import com.main.alumninetworkbackend.model.dbo.*;
import com.main.alumninetworkbackend.repository.TopicRepository;
import com.main.alumninetworkbackend.repository.AlumniUserRepository;
import com.main.alumninetworkbackend.util.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@Tag(name="Topic")
@RequestMapping(value="api/v1/")
@SecurityRequirement(name = "keycloak_implicit")
public class TopicController {

    private final TopicRepository topicRepository;
    public final AlumniUserRepository alumniUserRepository;


    public TopicController(TopicRepository topicRepository, AlumniUserRepository alumniUserRepository) {
        this.topicRepository = topicRepository;
        this.alumniUserRepository = alumniUserRepository;
    }


    @GetMapping("/topic/all")
    public ResponseEntity<CommonResponse> getAllTopics(HttpServletRequest request) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = topicRepository.findAll();
        commonResponse.message = "All topics";

        HttpStatus response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @GetMapping("/topic/{id}")
    public ResponseEntity<CommonResponse> getTopicById(HttpServletRequest request, @PathVariable int id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if (topicRepository.existsById(id)) {
            commonResponse.data = topicRepository.findById(id);
            commonResponse.message = "Topic ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Topic does not exist";
            response = HttpStatus.NOT_FOUND;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @PostMapping("/topic")
    public ResponseEntity<CommonResponse> createTopic(HttpServletRequest request, @RequestBody Topic topic) {
        Command cmd = new Command(request);

        topic = topicRepository.save(topic);

        CommonResponse cr = new CommonResponse();
        cr.data = topic;
        cr.message = "Created a new topic with ID: " + topic.id;

        HttpStatus response = HttpStatus.CREATED;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, response);
    }


    @PostMapping("/topic/{id}/join")
    public ResponseEntity<CommonResponse>  createMembershipRecord(HttpServletRequest request,  @RequestBody int userId , @PathVariable int id)  {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if (alumniUserRepository.existsById(userId) && topicRepository.existsById(id)) {

            Optional <AlumniUser> alumniUserOptional = alumniUserRepository.findById(userId);
            Optional <Topic> topicOptional = topicRepository.findById(id);

            AlumniUser alumniUser = alumniUserOptional.get();
            Topic topic = topicOptional.get();

            alumniUser.topics.add(topic);
            topic.alumniUsers.add(alumniUser);
            topicRepository.save(topic);
            alumniUserRepository.save(alumniUser);

            commonResponse.data = alumniUserRepository.findById(userId);
            commonResponse.message = "Added alumni user to topic with ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Topic does not exist";
            response = HttpStatus.NOT_FOUND;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);

    }

}

