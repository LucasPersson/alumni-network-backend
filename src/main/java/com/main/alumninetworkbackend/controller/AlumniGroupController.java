package com.main.alumninetworkbackend.controller;

import com.main.alumninetworkbackend.model.CommonResponse;
import com.main.alumninetworkbackend.model.dbo.AlumniGroup;
import com.main.alumninetworkbackend.model.dbo.AlumniUser;
import com.main.alumninetworkbackend.repository.AlumniGroupRepository;
import com.main.alumninetworkbackend.repository.AlumniUserRepository;
import com.main.alumninetworkbackend.util.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@Tag(name = "AlumniGroup")
@RequestMapping("api/v1/")
@SecurityRequirement(name = "keycloak_implicit")
public class AlumniGroupController {

    private final AlumniGroupRepository alumniGroupRepository;
    private final AlumniUserRepository alumniUserRepository;

    public AlumniGroupController(AlumniGroupRepository alumniGroupRepository, AlumniUserRepository alumniUserRepository) {
        this.alumniGroupRepository = alumniGroupRepository;
        this.alumniUserRepository = alumniUserRepository;
    }

    // Post an alumni group
    @PostMapping("/alumniGroup")
    public ResponseEntity<CommonResponse> createAlumniGroup(HttpServletRequest request, @RequestBody AlumniGroup alumniGroup){
        Command cmd = new Command(request);

        alumniGroup = alumniGroupRepository.save(alumniGroup);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = alumniGroup;
        commonResponse.message = "New alumni group with ID: " + alumniGroup.id;

        HttpStatus response = HttpStatus.CREATED;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read alumni groups by ID
    @GetMapping("/alumniGroup/{id}")
    public ResponseEntity<CommonResponse> getAlumniGroupById(HttpServletRequest request, @PathVariable int id){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(alumniGroupRepository.existsById(id)) {
            commonResponse.data = alumniGroupRepository.findById(id);
            commonResponse.message = "Alumni group ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Alumni group does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    // Read all alumni groups
    @GetMapping("/alumniGroup/all")
    public ResponseEntity<CommonResponse> getAllAlumniGroups(HttpServletRequest request){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = alumniGroupRepository.findAll();
        commonResponse.message = "All alumni groups";

        HttpStatus response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @PostMapping("/alumniGroup/{id}/join")
    public ResponseEntity<CommonResponse>  createMembershipRecord(HttpServletRequest request,  @RequestBody int userId , @PathVariable int id)  {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if (alumniUserRepository.existsById(userId) && alumniGroupRepository.existsById(id)) {

            Optional<AlumniUser> alumniUserOptional = alumniUserRepository.findById(userId);
            Optional <AlumniGroup> alumniGroupOptional = alumniGroupRepository.findById(id);

            AlumniUser alumniUser = alumniUserOptional.get();
            AlumniGroup alumniGroup = alumniGroupOptional.get();

            alumniUser.alumniGroups.add(alumniGroup);
            alumniGroup.alumniUsers.add(alumniUser);
            alumniGroupRepository.save(alumniGroup);
            alumniUserRepository.save(alumniUser);

            commonResponse.data = alumniUserRepository.findById(userId);
            commonResponse.message = "Added alumni user to group with ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Group does not exist";
            response = HttpStatus.NOT_FOUND;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);

    }

}
