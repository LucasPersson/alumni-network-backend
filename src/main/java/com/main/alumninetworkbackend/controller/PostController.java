package com.main.alumninetworkbackend.controller;

import com.main.alumninetworkbackend.model.CommonResponse;
import com.main.alumninetworkbackend.model.dbo.AlumniGroup;
import com.main.alumninetworkbackend.model.dbo.AlumniUser;
import com.main.alumninetworkbackend.model.dbo.Post;
import com.main.alumninetworkbackend.model.dbo.Topic;
import com.main.alumninetworkbackend.repository.AlumniGroupRepository;
import com.main.alumninetworkbackend.repository.AlumniUserRepository;
import com.main.alumninetworkbackend.repository.PostRepository;
import com.main.alumninetworkbackend.repository.TopicRepository;
import com.main.alumninetworkbackend.util.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@Tag(name = "Post")
@RequestMapping("api/v1/")
@SecurityRequirement(name = "keycloak_implicit")
public class PostController {

    private final PostRepository postRepository;
    private final AlumniGroupRepository alumniGroupRepository;
    private final TopicRepository topicRepository;
    private final AlumniUserRepository alumniUserRepository;

    public PostController(PostRepository postRepository, AlumniGroupRepository alumniGroupRepository, TopicRepository topicRepository, AlumniUserRepository alumniUserRepository) {
        this.postRepository = postRepository;
        this.alumniGroupRepository = alumniGroupRepository;
        this.topicRepository = topicRepository;
        this.alumniUserRepository = alumniUserRepository;
    }

    public Map<String, String> getUserInfo(@AuthenticationPrincipal Jwt principal) {
        Map<String, String> map = new Hashtable<String, String>();
        map.put("user_name", principal.getClaimAsString("preferred_username"));
        map.put("userSubToken", principal.getClaimAsString("sub"));

        return Collections.unmodifiableMap(map);
    }

    @GetMapping("/post/all")
    public ResponseEntity<CommonResponse> getAllPosts(HttpServletRequest request) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = postRepository.findAll();
        commonResponse.message = "All posts";

        HttpStatus response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @GetMapping("/post/alumniUser/{alumniUser_id}")
    public ResponseEntity<CommonResponse> getPostsByAlumniUser(HttpServletRequest request, @PathVariable int alumniUser_id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if (postRepository.existsById(alumniUser_id)) {

            Optional <AlumniUser> alumniUser = alumniUserRepository.findById(alumniUser_id);
            AlumniUser alumniBody = alumniUser.get();
            List<Post> postList = new ArrayList<>(alumniBody.posts);
            commonResponse.data = postList;
            commonResponse.message = "Posts from alumni user with ID: " + alumniUser_id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Posts from this alumni user do not exist";
            response = HttpStatus.NOT_FOUND;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @GetMapping("/post/alumniGroup/{alumniGroup_id}")
    public ResponseEntity<CommonResponse> getPostsInAlumniGroup(HttpServletRequest request, @PathVariable int alumniGroup_id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if (alumniGroupRepository.existsById(alumniGroup_id)) {

            Optional <AlumniGroup> alumniGroup = alumniGroupRepository.findById(alumniGroup_id);
            AlumniGroup alumniBody = alumniGroup.get();
            List<Post> postList = new ArrayList<>(alumniBody.posts);
            commonResponse.data = postList;
            commonResponse.message = "Posts from alumni group with ID: " + alumniGroup_id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Posts from this alumni group do not exist";
            response = HttpStatus.NOT_FOUND;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);

    }

    @GetMapping("/post/topic/{topic_id}")
    public ResponseEntity<CommonResponse> getPostsInTopic(HttpServletRequest request, @PathVariable int topic_id) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if (topicRepository.existsById(topic_id)) {
            Optional <Topic> topic = topicRepository.findById(topic_id);
            Topic topicBody = topic.get();
            List<Post> postList = new ArrayList<>(topicBody.posts);
            commonResponse.data = postList;
            commonResponse.message = "Posts by topic with ID: " + topic_id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Posts within this topic do not exist";
            response = HttpStatus.NOT_FOUND;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    @PostMapping("/post")
    public ResponseEntity<CommonResponse> createNewPost(HttpServletRequest request, @RequestBody Post post) {
        Command cmd = new Command(request);

        post = postRepository.save(post);

        CommonResponse commonResponse = new CommonResponse();
        commonResponse.data = post;
        commonResponse.message = "New post with ID: " + post.id;

        HttpStatus response = HttpStatus.CREATED;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }

    @PutMapping("/post/{id}")
    public ResponseEntity<CommonResponse> updatePost(HttpServletRequest request, @RequestBody Post updatePost, @PathVariable int id, @AuthenticationPrincipal Jwt principal) {

        Command cmd = new Command(request);
        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        Map<String, String> currentUserMap = getUserInfo(principal);
        String currentUserToken = currentUserMap.get("userSubToken");
        String currentUserUsername = currentUserMap.get("user_name");

        if (!alumniUserRepository.existsAlumniUserByKeyTokenIdAndUsername(currentUserToken,currentUserUsername)) {

            commonResponse.data = null;
            commonResponse.message = "Invalid user must be your own!";
            response = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(commonResponse, response);
        }

        Optional<AlumniUser> alumniUserRepositoryById = alumniUserRepository.getAlumniUserByKeyTokenIdAndUsername(currentUserToken, currentUserUsername);
        AlumniUser alumniUser = alumniUserRepositoryById.get();

        if(!postRepository.existsById(id)) {

            commonResponse.data = null;
            commonResponse.message = "Post does not exist!";
            response = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(commonResponse, response);

        }

        Optional<Post> franchiseRepositoryById = postRepository.findById(id);
        Post post = franchiseRepositoryById.get();

        if(post.alumniUser != alumniUser) {

            commonResponse.data = null;
            commonResponse.message = "Invalid user, must be your own!";
            response = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(commonResponse, response);
        }

        if(updatePost.title != null) {
            post.title = updatePost.title;
        }
        if(updatePost.content != null) {
            post.content = updatePost.content;
        }

        postRepository.save(post);

        commonResponse.data = post;
        commonResponse.message = "Updated post with ID: " + post.id;
        response = HttpStatus.OK;

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }



    @GetMapping("/post/{id}")
    public ResponseEntity<CommonResponse> getPostById(HttpServletRequest request, @PathVariable int id){
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        if(postRepository.existsById(id)) {
            commonResponse.data = postRepository.getPostByReplyParentIdOrId(id, id);
            commonResponse.message = "Post with ID: " + id;
            response = HttpStatus.OK;
        } else {
            commonResponse.data = null;
            commonResponse.message = "Post does not exist";
            response = HttpStatus.NOT_FOUND;
        }

        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }


    @GetMapping("/post/targetUser/{id}")
    public ResponseEntity<CommonResponse> getPostsDirectedAtUser(HttpServletRequest request, @PathVariable int id, @AuthenticationPrincipal Jwt principal) {
        Command cmd = new Command(request);

        CommonResponse commonResponse = new CommonResponse();
        HttpStatus response;

        Map<String, String> currentUserMap = getUserInfo(principal);
        String currentUserToken = currentUserMap.get("userSubToken");
        String currentUserUsername = currentUserMap.get("user_name");

        if(!alumniUserRepository.existsById(id)) {
            commonResponse.data = null;
            commonResponse.message = "User does not exist!";
            response = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(commonResponse, response);
        }

        Optional<AlumniUser> alumniUserRepositoryById = alumniUserRepository.findById(id);
        AlumniUser alumniUser = alumniUserRepositoryById.get();

        if (alumniUser.username.equals(currentUserUsername) && alumniUser.keyTokenId.equals(currentUserToken)) {

            List<Post> postList = postRepository.getPostsByTargetUser(id);
            postList.addAll(postRepository.getPostsByTargetUserIsNotNullAndAlumniUserId(id));
            List<Post> allPosts = new ArrayList<>();
            for( Post post : postList) {
               List<Post> tempPostList = postRepository.getPostByReplyParentIdOrId(post.id, post.id);
               for( Post tempPost : tempPostList) {
                   if (!allPosts.contains(tempPost)) {
                       allPosts.add(tempPost);
                   }
               }
            }
            commonResponse.data = allPosts;
            commonResponse.message = "Posts sent to user with ID: " + id;

            response = HttpStatus.OK;

        } else {
            commonResponse.data = null;
            commonResponse.message = "Invalid user, must be your own!";
            response = HttpStatus.FORBIDDEN;
        }
        cmd.setResult(response);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(commonResponse, response);
    }



}
