package com.main.alumninetworkbackend.model.dbo;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class AlumniUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false, length = 400)
    public String keyTokenId;

    @Column(nullable = false, length = 20)
    public String username;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    @Column(length = 100)
    public String name;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    @Column(length = 70)
    public String status;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    @Column(length = 200)
    public String bio;

    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    @Column(length = 200)
    public String funFact;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "alumniGroupMember",
            joinColumns = {@JoinColumn (name="user_id")},
            inverseJoinColumns = {@JoinColumn(name="group_id")}
    )
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<AlumniGroup> alumniGroups = new ArrayList<>();

    @JsonGetter("alumniGroups")
    public List<String> getAlumniGroupList(){
        return alumniGroups.stream()
                .map(alumniGroup -> {
                    return "/group/" + alumniGroup.id;
                }).collect(Collectors.toList());
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "topicMember",
            joinColumns = {@JoinColumn (name="user_id")},
            inverseJoinColumns = {@JoinColumn(name="topic_id")}
    )
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<Topic> topics = new ArrayList<>();

    @JsonGetter("topics")
    public List<String> getTopicsList(){
        return topics.stream()
                .map(topic -> {
                    return "/topic/" + topic.id;
                }).collect(Collectors.toList());
    }


    @OneToMany(mappedBy = "alumniUser", fetch = FetchType.LAZY)
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<Post> posts = new ArrayList<>();
    @JsonGetter("posts")
    public List<String> getPostList(){
        return posts.stream()
                .map(post -> {
                    return "/post/" + post.id;
                }).collect(Collectors.toList());
    }

}
