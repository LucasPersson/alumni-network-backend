package com.main.alumninetworkbackend.model.dbo;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class AlumniGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false, length = 20)
    public String name;

    @Column(nullable = false, length = 100)
    public String description;

    @Column(nullable = false, columnDefinition = "BOOLEAN DEFAULT false")
    public Boolean isPrivate;

    @ManyToMany(mappedBy = "alumniGroups", fetch = FetchType.LAZY)
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<AlumniUser> alumniUsers = new ArrayList<>();

    @JsonGetter("alumniUsers")
    public List<String> getAlumniUserList(){
        return alumniUsers.stream()
                .map(alumniUser -> {
                    return "/user/" + alumniUser.id;
                }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "alumniGroup", fetch = FetchType.LAZY)
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<Post> posts = new ArrayList<>();

    @JsonGetter("posts")
    public List<String> getPostList(){
        return posts.stream()
                .map(post -> {
                    return "/post/" + post.id;
                }).collect(Collectors.toList());
    }


}
