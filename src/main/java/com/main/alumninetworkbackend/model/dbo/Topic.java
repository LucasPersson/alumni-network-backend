package com.main.alumninetworkbackend.model.dbo;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(length = 20, nullable = false)
    public String name;

    @Column(length = 100, nullable = false)
    public String description;

    @ManyToMany(mappedBy = "topics", fetch = FetchType.LAZY)
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<AlumniUser> alumniUsers = new ArrayList<>();

    @JsonGetter("alumniUsers")
    public List<String> getAlumniUserList(){
        return alumniUsers.stream()
                .map(alumniUser -> {
                    return "/user/" + alumniUser.id;
                }).collect(Collectors.toList());
    }

    @OneToMany(mappedBy = "topic", fetch = FetchType.LAZY)
    @JsonInclude(value = JsonInclude.Include.NON_EMPTY)
    public List<Post> posts = new ArrayList<>();


    @JsonGetter("posts")
    public List<String> getPostList(){
        return posts.stream()
                .map(post -> {
                    return "/post/" + post.id;
                }).collect(Collectors.toList());
    }

}
