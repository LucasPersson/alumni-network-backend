package com.main.alumninetworkbackend.model.dbo;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @Column(nullable = false)
    public Timestamp timestamp;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer replyParentId;

    @Column
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Integer targetUser;

    @Column(nullable = false, length = 100)
    public String title;

    @Column(nullable = false, length = 450)
    public String content;

    @ManyToOne
    @JoinColumn(name = "alumnigroup_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public AlumniGroup alumniGroup;

    @JsonGetter("alumniGroup")
    public String getAlumniGroupList(){
        if(alumniGroup == null){
            return "";
        }else {
            return "/group/" + alumniGroup.id;
        }
    }


    @ManyToOne
    @JoinColumn(name = "alumniuser_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public AlumniUser alumniUser;

    @JsonGetter("alumniUser")
    public String getAlumniUserList(){
        return "/user/" + alumniUser.id;
    }


    @ManyToOne
    @JoinColumn(name = "topic_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Topic topic;

    @JsonGetter("topic")
    public String getTopicList(){
        if(topic == null){
            return "";
        }else {
            return "/topic/" + topic.id;
        }
    }

    @Override
    public int hashCode() {
        return id;
    }
    @Override
    public boolean equals(Object other) {
        return this.id == ((Post) other).id;
    }


}
