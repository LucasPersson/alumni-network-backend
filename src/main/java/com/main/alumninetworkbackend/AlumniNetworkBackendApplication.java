package com.main.alumninetworkbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlumniNetworkBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlumniNetworkBackendApplication.class, args);
    }

}
