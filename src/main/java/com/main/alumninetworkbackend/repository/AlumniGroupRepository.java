package com.main.alumninetworkbackend.repository;

import com.main.alumninetworkbackend.model.dbo.AlumniGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlumniGroupRepository extends JpaRepository<AlumniGroup, Integer> {
}
