package com.main.alumninetworkbackend.repository;

import com.main.alumninetworkbackend.model.dbo.AlumniUser;
import com.main.alumninetworkbackend.model.dbo.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Integer> {

    List<Post> getPostByReplyParentIdOrId(Integer replyId, Integer postId);
    List<Post> getPostsByTargetUser(Integer id);
    List<Post> getPostsByTargetUserIsNotNullAndAlumniUserId(Integer id);
}
