package com.main.alumninetworkbackend.repository;

import com.main.alumninetworkbackend.model.dbo.AlumniUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AlumniUserRepository extends JpaRepository<AlumniUser, Integer> {
    Boolean existsAlumniUserByKeyTokenIdAndUsername(String tokenId, String username);
    Optional<AlumniUser> findAlumniUserByKeyTokenIdAndUsername(String tokenId, String username);
    Optional<AlumniUser> getAlumniUserByKeyTokenIdAndUsername(String tokenId, String username);
}
