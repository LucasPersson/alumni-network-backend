package com.main.alumninetworkbackend.repository;

import com.main.alumninetworkbackend.model.dbo.Topic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicRepository extends JpaRepository<Topic, Integer> {
}
